import random

class GeneticAlgorithm:
    def __init__(self):
        """
        A genetic algorithm base
        """
        self.population = None
    
    def init_population(self):
        
    
    @staticmethod
    def crossover(gene_a, gene_b):
        """
        Perform crossover on two genes
        
        Parameters
        ------------
        gene_a  : list
            First gene
        gene_b  : list
            Second gene
        
        Returns
        ------------
        tuple
            The two child genes
        """
        point = random.uniform(0, round(len(gene_a) / 2))
        gene_c = gene_a[:point] + gene_b[point:]
        gene_d = gene_b[:point] + gene_a[point:]
        
        return gene_c, gene_d
        
    @staticmethod
    def roulette_wheel(candidates):
        """
        Roulette wheel selection for the candidates
        
        Parameters
        ------------
        candidates  : dict
            A dictionary with fitness as key and genes as value
        
        Returns
        ------------
        tuple
            The selected fitness and gene
        """
        probs = sorted(list(candidates.keys()))
        n = len(probs)
        A = [0]*n
        A[0] = probs[0]
        scaled_map = {}
        scaled_map[A[0]] = probs[0]

        for i in range(1,len(probs)):
            A[i] = A[i - 1] + probs[i]
            scaled_map[A[i]] = probs[i]
        
        pick = random.uniform(0,max(A))

        while True:
            mid = round(len(A) / 2)
            
            if (A[mid] > pick and A[mid-1] < pick) or len(A) == 1:
                return scaled_map[A[mid]], candidates[scaled_map[A[mid]]]
            elif pick > A[mid]:
                A = A[mid:]
            else:
                A = A[:mid]
